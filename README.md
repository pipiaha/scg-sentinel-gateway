# scg-sentinel-gateway

网关

## 1. sentinel + nacos 支持

### 1.1 pom

增加如下插件：

```xml

        <!--   sentinel和spring-cloud-gateway(scg)整合     -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-alibaba-sentinel-gateway</artifactId>
            <version>2.1.1.RELEASE</version>
        </dependency>
        <!--  sentinel客户端      -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
            <version>2.1.1.RELEASE</version>
        </dependency>
        <!--
            为spring-cloud-gateway(scg)提供默认配置 
            如 com.alibaba.cloud.sentinel.gateway.scg.SentinelSCGAutoConfiguration
        -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-spring-cloud-gateway-adapter</artifactId>
            <version>1.8.2</version>
        </dependency>
        <!-- sentinel 数据源nacos支持 -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-datasource-nacos</artifactId>
            <version>1.8.2</version>
        </dependency>
        <!-- sentinel控制台metric支持(实时监控) -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-transport-simple-http</artifactId>
            <version>1.6.3</version>
        </dependency>
```

---

### 1.2 configuration

1. 增加工程类型配置（非必须）

新增文件`classpath:sentinel.properties` ，定义此工程类型为`spring cloud gateway`(=11)，流控使用网关流控模式；默认`appType = 0`（APP_TYPE_COMMON)，流控使用默认处理方式。

全部的配置项参考：[sentinel配置](https://github.com/alibaba/Sentinel/wiki/%E5%90%AF%E5%8A%A8%E9%85%8D%E7%BD%AE%E9%A1%B9)

配置读取流程：

`com.alibaba.csp.sentinel.config.SentinelConfig` 通过
`com.alibaba.csp.sentinel.config.SentinelConfigLoader`依次扫描:
* `-Dcsp.sentinel.config.file=${filename}` 指定的`${filename}`文件；
* `classpath:sentinel.properties`文件 **（当前采用方式）**
* `logs/scp/`目录下 `${工程名}.properties`文件
* 启动参数 `System.getProperties()`

找到所有配置项，加载和sentinel相关内容。所有可配置项如下：

```java
    public static final String APP_TYPE = "csp.sentinel.app.type";
    public static final String CHARSET = "csp.sentinel.charset";
    public static final String SINGLE_METRIC_FILE_SIZE = "csp.sentinel.metric.file.single.size";
    public static final String TOTAL_METRIC_FILE_COUNT = "csp.sentinel.metric.file.total.count";
    public static final String COLD_FACTOR = "csp.sentinel.flow.cold.factor";
    public static final String STATISTIC_MAX_RT = "csp.sentinel.statistic.max.rt";
    public static final String SPI_CLASSLOADER = "csp.sentinel.spi.classloader";

```

网关流控模式下，`路由配置中的id`和`API组名称`，都会被当做`资源(resource)`使用，所有归于此路由/API组下的请求都会被`Sentinel`一起统计，
可参考官方文档 [网关限流](https://github.com/alibaba/Sentinel/wiki/%E7%BD%91%E5%85%B3%E9%99%90%E6%B5%81#spring-cloud-gateway) 部分；

网关模式的控制台 **不支持** ：

- 流量控制（FlowRule)、
- 热点规则（ParamRule)、
- 授权规则（AuthorityRule）

可通过修改`appType`来改变节点类型进而切换控制台模式。

---
scg定义如下：

```java

package com.alibaba.cloud.sentinel.gateway;

/**
 * @author <a href="mailto:fangjian0423@gmail.com">Jim</a>
 */
public interface ConfigConstants {

	String APP_TYPE_ZUUL_GATEWAY = "12";
	String APP_TYPE_SCG_GATEWAY = "11";

	String ZUUl_PREFIX = "spring.cloud.sentinel.zuul";

	String GATEWAY_PREFIX = "spring.cloud.sentinel.scg";

	String FALLBACK_MSG_RESPONSE = "response";
	String FALLBACK_REDIRECT = "redirect";

}

```
---

2. 增加`sentinel`配置

相关 java class ： `com.alibaba.cloud.sentinel.SentinelProperties`

```yaml
spring:
  cloud:
    # 其他配置略...
    
    sentinel:
      enabled: true
      # spring cloud gateway
      scg: 
        # 对应FallbackProperties类，http-status默认429，content-type默认 application/json
        fallback: # 服务流控/异常/降级之后处理，默认使用BlockRequestHandler
          mode: response # 使用响应结果方式，还有redirect重定向方式
          response-body:  '{"code": 40000, "message": "服务忙，请稍等"}' # 返回的结果，默认json格式通过content-type定义
      # dashboard
      transport:
        dashboard: localhost:18080 # sentinel控制台地址
      # sentinel增加nacos 数据源
      datasource:
        ds-authority-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-authority-rules # 名称在 grap-sentinel-dashboard定义，前缀默认为sentinel
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: authority # 规则类型枚举参见 com.alibaba.cloud.sentinel.datasource.RuleType
        ds-degrade-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-degrade-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: degrade # 熔断规则
        ds-flow-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-flow-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: flow # 流控规则
        ds-param-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-param-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: param-flow # 热点参数
        ds-system-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-system-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: system #系统规则
        ds-gateway-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-gateway-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: gw-flow # 网关流控
        ds-api-rules:
          nacos:
            server-addr: ${spring.cloud.nacos.discovery.server-addr}
            data-id: sentinel-api-rules
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: gw-api-group # API 分组
      eager: true # 控制台懒加载

```
---


### 1.3 examples

### 1.4 动态规则
