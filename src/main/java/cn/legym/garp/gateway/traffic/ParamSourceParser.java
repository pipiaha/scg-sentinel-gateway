package cn.legym.garp.gateway.traffic;

import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * create by pipisun on 2021/9/6
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 * last update by {} on {}
 * update description:
 */
public interface ParamSourceParser {

    String parse(ServerHttpRequest request, String key);
}
