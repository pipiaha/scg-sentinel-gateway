package cn.legym.garp.gateway.traffic;


import cn.legym.garp.gateway.traffic.param.HeaderParamParser;
import cn.legym.garp.gateway.traffic.param.RequestBodyParamParser;
import cn.legym.garp.gateway.traffic.param.RestfulPathParamParser;
import cn.legym.garp.gateway.traffic.param.UrlParamParser;

/**
 * create by pipisun on 2021/9/6
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 * <p>
 * 参数匹配枚举
 *
 * <li>URL: 从请求路径中提取参数，适用GET请求带参数的场景。
 * <p>
 * e.g.
 * 请求 https://some.web.site/path/to/res?foo=42&bar=123 ,key为参数名，如foo</li>
 * <li>Restful: 从请求路径中获得内容，使用正则匹配，分组捕获内容
 *
 * <p>
 * e.g.
 * 请求 https://some.web.site/rest/path/res_id/123
 * key为正则表达式： ^/rest/path/res_id/(.*)/?
 *
 * <p>
 * 注意： Java中正则表达式 \符号 需要额外转义，应写为 \\
 * </p>
 * </li>
 *
 * <li>
 * RequestBody: 从请求body中提取内容。将JSON格式的body解析为Map&lt;String,String&gt;,
 * 用key 获得对应值。（暂不支持JSON格式嵌套，key只能在第一层查找）
 *
 * </li>
 * <li>
 * Header: 在请求的header中找到对应键值，key为header键。
 * </li>
 * last update by {} on {}
 * update description:
 */
public enum ParamSourceType {


    URL(UrlParamParser.class),
    RESTFUL_PATH(RestfulPathParamParser.class),
    REQUEST_BODY_JSON(RequestBodyParamParser.class),
    HEADER(HeaderParamParser.class),
    ;

    private final Class<? extends ParamSourceParser> parserClazz;


    ParamSourceType(Class<? extends ParamSourceParser> clazz) {
        this.parserClazz = clazz;
    }

    public Class<? extends ParamSourceParser> getParserClazz() {
        return parserClazz;
    }
}
