package cn.legym.garp.gateway.traffic.config;

import java.util.Collection;

/**
 * create by pipisun on 2021/9/7
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 *
 * 自定义规则配置加载监听器
 *
 *
 * last update by {} on {}
 * update description:
 */
public interface TrafficDataSourceListener<S> {

    void updateData(Collection<S> source);
}
