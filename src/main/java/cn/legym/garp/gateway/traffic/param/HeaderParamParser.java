package cn.legym.garp.gateway.traffic.param;

import cn.legym.garp.gateway.traffic.ParamSourceParser;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * create by pipisun on 2021/9/6
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 * last update by {} on {}
 * update description:
 */
@Component
public class HeaderParamParser implements ParamSourceParser {

    @Override
    public String parse(ServerHttpRequest request, String key) {
        List<String> values = request.getHeaders().get(key.trim());
        return CollectionUtils.isEmpty(values) ? null : values.get(0);
    }
}
