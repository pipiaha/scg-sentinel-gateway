package cn.legym.garp.gateway.traffic.param;

import cn.legym.garp.gateway.traffic.ParamSourceParser;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * create by pipisun on 2021/9/7
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 * last update by {} on {}
 * update description:
 */
@Component
public class RestfulPathParamParser implements ParamSourceParser {

    @Override
    public String parse(ServerHttpRequest request, String key) {
        Pattern pattern = Pattern.compile(key);
        Matcher matcher = pattern.matcher(request.getPath().value());
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return null;
    }
}
