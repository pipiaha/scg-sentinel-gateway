package cn.legym.garp.gateway.traffic.dynamic.watcher;

import cn.legym.garp.gateway.traffic.dynamic.SentinelDatasourceWatcher;
import com.google.common.collect.Sets;

import java.util.Set;

public abstract class AbstractSentinelDatasourceWatcher implements SentinelDatasourceWatcher {

    private final Set<InternalSentinelListener> listeners = Sets.newConcurrentHashSet();

    @Override
    public SentinelDatasourceWatcher addListener(InternalSentinelListener listener) {
        listeners.remove(listener);
        listeners.add(listener);
        return this;
    }

    @Override
    public SentinelDatasourceWatcher removeListener(InternalSentinelListener listener) {
        this.listeners.remove(listener);
        return this;
    }

    @Override
    public Set<InternalSentinelListener> getListeners() {
        return listeners;
    }
}
