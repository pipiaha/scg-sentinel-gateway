package cn.legym.garp.gateway.traffic.dynamic.bean;

import com.alibaba.cloud.sentinel.datasource.RuleType;
import lombok.Data;

/**
 * create by pipisun on 2021/9/6
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 *
 * last update by {} on {}
 * update description:
 *
 * @see  com.alibaba.csp.sentinel.slots.block.flow.FlowRule
 * @see  com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule
 * @see  com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager
 * @see  com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule
 * @see  com.alibaba.csp.sentinel.slots.system.SystemRule
 * @see  com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule
 * @see  com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition
 */
@Data
public class RuleMeta {

    /**
     * Sentinel 规则类型
     */
    private RuleType ruleType;

    /**
     * json 格式规则内容，对应{@link RuleMeta#ruleType}
     */
    private String content;
}
