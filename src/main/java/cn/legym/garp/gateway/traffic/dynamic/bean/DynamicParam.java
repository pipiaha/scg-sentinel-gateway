package cn.legym.garp.gateway.traffic.dynamic.bean;

import cn.legym.garp.gateway.traffic.ParamSourceType;
import lombok.Data;

/**
 * create by pipisun on 2021/9/6
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 *
 * 参数匹配定义
 *
 * last update by {} on {}
 * update description:
 */
@Data
public class DynamicParam {

    private ParamSourceType from;

    private String key;
}
