package cn.legym.garp.gateway.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * create by 这个婆娘不寻常 on 2019/3/14
 * copyright © 2017-2019 Legym Technology Co.,Ltd. All rights reserved.
 * file description:
 * last update by {} on {}
 * update description:
 */

@Configuration
public class HealthCheckConfiguration {

    @Bean
    public RouterFunction<ServerResponse> testFunRouterFunction() {
        return RouterFunctions.route(
                RequestPredicates.path("/"),
                request -> ServerResponse.ok().body(BodyInserters.fromObject("UP")));
    }
}
