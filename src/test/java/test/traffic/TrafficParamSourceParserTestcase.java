package test.traffic;

import cn.legym.garp.gateway.traffic.ParamSourceParser;
import cn.legym.garp.gateway.traffic.param.HeaderParamParser;
import cn.legym.garp.gateway.traffic.param.RequestBodyParamParser;
import cn.legym.garp.gateway.traffic.param.RestfulPathParamParser;
import cn.legym.garp.gateway.traffic.param.UrlParamParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;

import java.util.Date;

/**
 * create by pipisun on 2021/9/7
 * copyright © 2017-2021 Legym Technology Co.,Ltd. All rights reserve
 * d.
 * file description:
 * last update by {} on {}
 * update description:
 */
public class TrafficParamSourceParserTestcase {

    @Test
    public void testUrlParamParser() {
        ParamSourceParser parser = new UrlParamParser();
        MockServerHttpRequest request = MockServerHttpRequest.get("http://some.web.site")
                .queryParam("concern", "25")
                .queryParam("certain", "88")
                .build();

        Assert.assertEquals(parser.parse(request, "concern"), "25");
        Assert.assertEquals(parser.parse(request, "certain"), "88");
        Assert.assertNull(parser.parse(request, "notExists"));
    }

    @Test
    public void testRequestBodyParamParser() throws JsonProcessingException {
        Date now = new Date();
        TestRequestBodyPayload payload = TestRequestBodyPayload.builder()
                .type(RequireType.LARGE)
                .name("payloader")
                .time(now)
                .value(1234L)
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        ParamSourceParser parser = new RequestBodyParamParser();
        MockServerHttpRequest request = MockServerHttpRequest.post("http://some.web.site")
                .body(objectMapper.writeValueAsString(payload));
        Assert.assertEquals(parser.parse(request, "name"), payload.getName());
        Assert.assertEquals(parser.parse(request, "type"), payload.getType().name());
    }

    @Test
    public void testRestfulPathParamParser() {
        ParamSourceParser parser = new RestfulPathParamParser();
        MockServerHttpRequest request = MockServerHttpRequest.get("http://some.web.site/restful/123")
                .build();

        Assert.assertEquals(parser.parse(request, "/restful/(.*)/?"), "123");
    }

    @Test
    public void testHeaderParamParser() {
        ParamSourceParser parser = new HeaderParamParser();
        MockServerHttpRequest request = MockServerHttpRequest.get("http://some.web.site/restful/123")
                .header("test", "testvalue")
                .build();
        Assert.assertEquals(parser.parse(request, "test"), "testvalue");
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class TestRequestBodyPayload {

        private String name;
        private RequireType type;
        private Long value;
        private Date time;
    }

    private enum RequireType {
        LARGE,
        SMALL,
        MINI,
        ;
    }

}
